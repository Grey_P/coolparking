﻿using CoolParking.BL.Interfaces;
using System.Timers;
using CoolParking.BL.Models;
// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer timer;

        private double interval;
        public double Interval
        {
            get
            {
                return this.interval;
            }
            set
            {
                interval = value;
            }
        }
        /*
        private TimerService()
        {
            this.Elapsed += this.Elapsed;
            this.Interval = interv;
            this.timer = new System.Timers.Timer(this.Interval);
            this.timer.Elapsed += this.Elapsed;
        }
        */
        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            this.timer.Dispose();
        }

        public void Start()
        {
            timer = new System.Timers.Timer(this.Interval);
            timer.Elapsed += this.Elapsed;
            timer.Start();
        }

        public void Stop()
        {
            this.timer.Stop();
        }
    }

}