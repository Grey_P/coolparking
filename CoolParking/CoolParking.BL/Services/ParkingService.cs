﻿using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CoolParking.BL.Models;
using System.IO;
// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        //private static ParkingService instance;
        private List<TransactionInfo> transactionInfos;
        private Parking parking;
        private ITimerService withdrawTimer;
        private ITimerService logTimer;
        private ILogService logService;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            transactionInfos = new List<TransactionInfo>();

            this.withdrawTimer = withdrawTimer;
            this.withdrawTimer.Elapsed += withdrawEvent;
            this.withdrawTimer.Interval = Settings.payPeriod * 1000;
            this.withdrawTimer.Start();

            this.logTimer = logTimer;
            this.logTimer.Elapsed += logEvent;
            this.logTimer.Interval = Settings.writeToLogPeriod * 1000;
            this.logTimer.Start();

            this.logService = logService;

            this.parking = Parking.getInstance();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            int index = this.parking.vehicleCollection.FindIndex(a => a.Id == vehicle.Id);

            if (index != -1)
            {
                throw new System.ArgumentException();
            }
            if (parking.vehicleCollection.Count >= Settings.capacity)
            {
                throw new System.InvalidOperationException();
            }
            parking.vehicleCollection.Add(vehicle);
        }

        public void Dispose()
        {
            withdrawTimer.Dispose();
            logTimer.Dispose();
            parking.vehicleCollection = new List<Vehicle>();
            transactionInfos = new List<TransactionInfo>();
            parking.balance = 0;
        }

        public decimal GetBalance()
        {
            return this.parking.balance;
        }

        public int GetCapacity()
        {
            return Settings.capacity;
        }

        public int GetFreePlaces()
        {
            return Settings.capacity - parking.vehicleCollection.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            if (this.transactionInfos.Count > 0)
            {
                TransactionInfo[] rez = new TransactionInfo[transactionInfos.Count];
                for (int i = 0; i < transactionInfos.Count; i++)
                {
                    rez[i] = transactionInfos[i];
                }
                return rez;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(parking.vehicleCollection);
        }

        public string ReadFromLog()
        {
            return this.logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            int index = this.parking.vehicleCollection.FindIndex(a => a.Id == vehicleId);
            if (index == -1)
            {
                throw new ArgumentException();
            }
            if (this.parking.vehicleCollection[index].Balance < 0)
            {
                throw new System.InvalidOperationException();
            }
            parking.vehicleCollection.RemoveAt(index);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
            {
                throw new ArgumentException();
            }

            int index = this.parking.vehicleCollection.FindIndex(a => a.Id == vehicleId);
            if (index == -1)
            {
                throw new ArgumentException();
            }
            else
            {
                this.parking.vehicleCollection[index].Balance += sum;
            }
        }

        private void withdrawEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            for (int i = 0; i < parking.vehicleCollection.Count; i++)
            {
                TransactionInfo inf = new TransactionInfo();
                inf.Id = parking.vehicleCollection[i].Id;
                inf.time = DateTime.Now;
                if (parking.vehicleCollection[i].Balance >= Settings.costOfParking[parking.vehicleCollection[i].VehicleType])
                {
                    inf.Sum = Settings.costOfParking[parking.vehicleCollection[i].VehicleType];
                }
                else if (parking.vehicleCollection[i].Balance <= 0)
                {
                    inf.Sum = Settings.costOfParking[parking.vehicleCollection[i].VehicleType] * Settings.fineCoeff;
                }
                else
                {
                    // price = balans + (price - balans) * koef
                    inf.Sum = parking.vehicleCollection[i].Balance + (Settings.costOfParking[parking.vehicleCollection[i].VehicleType] - parking.vehicleCollection[i].Balance) * Settings.fineCoeff;
                }
                transactionInfos.Add(inf);
                parking.vehicleCollection[i].Balance -= inf.Sum;
                this.parking.balance += inf.Sum;
            }
        }

        public void logEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            logService.Write("");
            for (int i = 0; i < transactionInfos.Count; i++)
            {

                string str = "Id:" + transactionInfos[i].Id + "\nSum:" + transactionInfos[i].Sum + "\nTime:" + transactionInfos[i].time + "\n";
                logService.Write(str);
            }
            transactionInfos.Clear();
        }
    }

}