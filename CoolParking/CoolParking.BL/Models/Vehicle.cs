﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id;
        public VehicleType VehicleType;
        public decimal Balance;

        public Vehicle(string id, VehicleType vt, decimal b)
        {
            if (isValid(id) && b >= 0)
            {
                this.Id = id;
                this.VehicleType = vt;
                this.Balance = b;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public bool isValid(string id)
        {
            Regex regex = new Regex("^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
            return regex.IsMatch(id);
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            string s = "";
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var numbers = "0123456789";
            var random = new Random();

            // add 2 symbol
            s += chars[random.Next(chars.Length)];
            s += chars[random.Next(chars.Length)];
            s += "-";
            for (int i = 0; i < 4; i++)
            {
                s += numbers[random.Next(chars.Length)];
            }
            s += "-";
            s += chars[random.Next(chars.Length)];
            s += chars[random.Next(chars.Length)];
            return s;
        }
    }
}