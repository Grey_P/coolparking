﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;
namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal startBalance = 0;
        public const int capacity = 10;
        public const int payPeriod = 5;
        public const int writeToLogPeriod = 60;

        // ? i`m not sure about it
        public static Dictionary<VehicleType, decimal> costOfParking = new Dictionary<VehicleType, decimal>
    {
        {VehicleType.PassengerCar, 2.0M},
        {VehicleType.Truck, 5.0M },
        {VehicleType.Bus, 3.5M },
        {VehicleType.Motorcycle, 1.0M }
    };
        public const decimal fineCoeff = 2.5M;
    }

}