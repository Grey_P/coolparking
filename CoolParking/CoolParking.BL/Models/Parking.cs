﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;
namespace CoolParking.BL.Models
{
    public class Parking
    {
        public static Parking inst;
        public decimal balance;
        public List<Vehicle> vehicleCollection;

        private Parking()
        {
            this.balance = Settings.startBalance;
            vehicleCollection = new List<Vehicle>();
        }

        public static Parking getInstance()
        {
            if (inst == null)
            {
                return inst = new Parking();
            }
            else
            {
                return inst;
            }
        }
    }
}