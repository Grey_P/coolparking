﻿using System;
using System.Collections.ObjectModel;
using CoolParking.BL.Models;
//using CoolParking.BL.Models;

namespace CoolParking.BL.Interfaces
{
    public interface IParkingService : IDisposable
    {
        decimal GetBalance(); // Отримати баланс
        int GetCapacity(); // Отримати кількість місць Всього
        int GetFreePlaces(); // Отримати кількість вільних місць
        ReadOnlyCollection<Vehicle> GetVehicles(); //Отримати інфо про авто на парковці
        void AddVehicle(Vehicle vehicle); // Додавати авто
        void RemoveVehicle(string vehicleId); // Видаляти авто
        void TopUpVehicle(string vehicleId, decimal sum); // Поповнювати баланс
        TransactionInfo[] GetLastParkingTransactions();
        string ReadFromLog(); // Отрима весь log
        /*
         * 
         *Вивести на екран поточний баланс Паркінгу.
            Вивести на екран суму зароблених коштів за поточний період (до запису у лог).
        Вивести на екран кількість вільних місць на паркуванні (вільно X з Y).
        Вивести на екран усі Транзакції Паркінгу за поточний період (до запису у лог).
Вивести на екран історію Транзакцій (зчитавши дані з файлу Transactions.log).
Вивести на екран список Тр. засобів , що знаходяться на Паркінгу.
Поставити Транспортний засіб на Паркінг.
Забрати Транспортний засіб з Паркінгу.
Поповнити баланс конкретного Тр. засобу.
         */
    }
}
