﻿using System;
using System.Collections.ObjectModel;
using CoolParking.BL.Services;
using CoolParking.BL.Models;

namespace CoolParking.Application
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu menu = new Menu();

            while (menu.exit)
            {
                menu.Menuintroduction();   
            }
        }
    }

    public class Menu
    {
        private ParkingService parkingservice = new ParkingService(new TimerService(), new TimerService(), new LogService());
        public bool exit = true; 
        public void Menuintroduction()
        {
            // all Ukrainian i replaced by eng i
            Console.WriteLine("-----< Hello! >-----");
            Console.WriteLine("Please, chose action:");
            Console.WriteLine("1.Поставити транспортний засiб на парковку.");
            Console.WriteLine("2.Забрати транспортний засiб.");
            Console.WriteLine("3.Поповнити баланс конкретного Тр. засобу.");
            Console.WriteLine("4.Вивести на екран кiлькiсть вiльних мiсць на паркуваннi (вiльно X з Y).");
            Console.WriteLine("5.Вивести на екран список Тр. засобiв , що знаходяться на Паркiнгу.");
            Console.WriteLine("6.Вивести на екран iсторiю Транзакцiй.");
            Console.WriteLine("7.Вивести на екран усi Транзакцiї Паркiнгу за поточний перiод (до запису у лог).");
            Console.WriteLine("8.Вивести на екран суму зароблених коштiв за поточний перiод (до запису у лог).");
            Console.WriteLine("9.Вивести на екран поточний баланс паркiнгу.");
            Console.WriteLine("0.Завершити сеанс.");
            Console.Write("Chose number--->  ");
            var val = Console.ReadLine();
            try
            {
                int a = int.Parse(val);
                if(a >= 0 && a <= 9)
                {
                    Console.Clear();
                    switch (a) {
                        case 1:
                            addTransportToParking();
                            break;
                        case 2:
                            removeTransportFromParking();
                            break;
                        case 3:
                            uppMoneyToTransport();
                            break;
                        case 4:
                            outFreePlaces();
                            break;
                        case 5:
                            outVehiclesOnParking();
                            break;
                        case 6:
                            outTransactionHistory();
                            break;
                        case 7:
                            outTransactionBeforeLog();
                            break;
                        case 8:
                            outIncomeBeforeLog();
                            break;
                        case 9:
                            outBalanceAtTheMoment();
                            break;
                        case 0:
                            Stop();
                            break;

                    }
                    Console.Clear();

                } else
                {
                    Console.Clear();
                    Console.WriteLine("Next time enter number from 1 to 9 pleeeease :)");
                    Console.ReadKey();
                    Console.Clear();
                }
            } catch
            {
                Console.Clear();
                Console.WriteLine("Incorrect input :( ");
                Console.ReadKey();
                Console.Clear();
                return;
            }
        }

        void addTransportToParking()
        {
            string id;
            VehicleType type = new VehicleType();
            Console.WriteLine("Введiть id транспорту(в форматi AB-1234-CD ):");
            Console.Write("--> ");
            id = Console.ReadLine();

            // VenicleType
            Console.WriteLine("Оберiть тип транспорту:");
            Console.WriteLine("1.PassengerCar");
            Console.WriteLine("2.Truck");
            Console.WriteLine("3.Bus");
            Console.WriteLine("4.Motorcycle");
            Console.Write("--> ");
            var readedNumberOfType = Console.ReadLine();

            int numberVehicleType = int.Parse(readedNumberOfType);
            if (numberVehicleType >= 1 && numberVehicleType <= 4)
            {
                switch (numberVehicleType)
                {
                    case 1:
                        type = VehicleType.PassengerCar;
                        break;
                    case 2:
                        type = VehicleType.Truck;
                        break;
                    case 3:
                        type = VehicleType.Bus;
                        break;
                    case 4:
                        type = VehicleType.Motorcycle;
                        break;
                }
            }
            else
            {
                Console.WriteLine("I don`t know what is type of your transport(");
                Console.ReadKey();
                Console.Clear();
                return;
            }
            // Balance
            Console.WriteLine("Введiть початковий баланс:");
            Console.Write("--> ");
            decimal startBal = decimal.Parse(Console.ReadLine());
            parkingservice.AddVehicle(new Vehicle(id, type, startBal));
        }

        void removeTransportFromParking()
        {
            Console.WriteLine("Введiть id транспорту який забирають з паркiнгу:");
            Console.Write("--> ");
            string id = Console.ReadLine();
            parkingservice.RemoveVehicle(id);
        }

        void uppMoneyToTransport()
        {
            Console.WriteLine("Введiть id транспорту для якого поповнюємо баланс:");
            Console.Write("--> ");
            string id = Console.ReadLine();
            Console.WriteLine("Введiть сумму поповнення:");
            Console.Write("--> ");
            decimal upBal = decimal.Parse( Console.ReadLine());
            parkingservice.TopUpVehicle(id,upBal);
        }
    
        void outFreePlaces()
        {
            Console.WriteLine("Вiльно: " + parkingservice.GetFreePlaces() + " з " + Settings.capacity);
            Console.ReadKey();
        }

        void outVehiclesOnParking()
        {
            ReadOnlyCollection<Vehicle> vehicles = parkingservice.GetVehicles();
            if(vehicles.Count == 0)
            {
                Console.WriteLine("На парковцi наразi жодного тс.засобу");
                Console.ReadKey();
                return;
            }
            for(int i = 0;i < vehicles.Count; i++)
            {
                Console.WriteLine("Id: " + vehicles[i].Id + ". Type: " + vehicles[i].VehicleType + ". Balance: " + vehicles[i].Balance + "\n");
            }
            Console.ReadKey();
        }
    
        void outTransactionHistory()
        {
            Console.WriteLine("History:\n" + parkingservice.ReadFromLog());
            Console.ReadKey();
        }

        void outTransactionBeforeLog()
        {
            try
            {
                TransactionInfo[] trsctInfo = parkingservice.GetLastParkingTransactions();
                for (int i = 0; i < trsctInfo.Length; i++)
                {
                    Console.WriteLine("ID: " + trsctInfo[i].Id + ". Sum: " + trsctInfo[i].Sum + ". Time: " + trsctInfo[i].time);
                }
                Console.ReadKey();

            }
             catch
            {
                Console.WriteLine("В поточному перiодi транзакцiї не вiдбувалися.");
                Console.ReadKey();
            }
        }

        void outIncomeBeforeLog()
        {
            try
            {
                TransactionInfo[] trsctInfo = parkingservice.GetLastParkingTransactions();
                decimal sum = 0;
                for (int i = 0; i < trsctInfo.Length; i++)
                {
                    sum += trsctInfo[i].Sum;
                }
                Console.WriteLine("Заробленi кошти за поточний перiод: " + sum);
                Console.ReadKey();
            } catch
            {
                Console.WriteLine("В поточному перiодi транзакцiї не проводились");
                Console.ReadKey();
            }
        }

        void outBalanceAtTheMoment()
        {
            Console.WriteLine("Поточний баланс паркiнгу: " + parkingservice.GetBalance());
            Console.ReadKey();
        }

        void Stop()
        {
            exit = false;
            Console.WriteLine("Good luck)))");
            Console.ReadKey();
        }
    }
}
